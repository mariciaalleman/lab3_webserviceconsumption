package com.maricia.webserviceconsumption;

import android.content.Intent;
import android.media.session.PlaybackState;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private EditText customerIdEditText;  //customerid
    private EditText customerFirstNameEditText; //customer first name
    private EditText customerLastNameEditText; // customer last name
    private EditText customerCityEditText; //customer city
    private EditText customerStreetEditText; // customer street
    private TextView customerNumTextView; //customer id

    private Button pullBtn; // pull
    //xml tag names
    static final String FIRSTNAME = "FIRSTNAME";
    static final String LASTNAME = "LASTNAME";
    static final String STREET = "STREET";
    static final String CITY = "CITY";


    int customerID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GetEditTextFields();
        SetTextEditable();
    }

    public void GetEditTextFields(){
        customerIdEditText = this.findViewById(R.id.customerIdEditText);
        customerFirstNameEditText = this.findViewById(R.id.customerFirstNameEditText);
        customerLastNameEditText = this.findViewById(R.id.customerLastNameEditText);
        customerCityEditText = this.findViewById(R.id.customerCityEditText);
        customerStreetEditText = this.findViewById(R.id.customerStreetEditText);
        customerNumTextView = this.findViewById(R.id.customerNumTextView);

    }

    private void SetTextEditable(){
        customerIdEditText.setEnabled(true);
        customerFirstNameEditText.setEnabled(false);
        customerLastNameEditText.setEnabled(false);
        customerStreetEditText.setEnabled(false);
        customerCityEditText.setEnabled(false);
    }

    private void GetButtons(){
        pullBtn = this.findViewById(R.id.pullBtn);
    }


    //button
    public void onClick(View arg0){
        GetButtons();
        pullBtn.setEnabled(false); //disable the button
        customerIdEditText.setEnabled(false); //disable the customer number input
        new LoadWebURL().execute(); //call inner class
    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //allows menu to work
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }


    //menu items
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId() == R.id.action_about){
            //TODO do something when clicked
            Intent intent =  new Intent (this, AboutActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //inner class
    private class LoadWebURL extends AsyncTask<Void, Void, Map<String, String>> {
        boolean notthere = false;
        Map<String, String> customerInfo = new HashMap<>();  //stores the customer info

       //method to read in website
        protected String readStream(InputStream in){

            StringBuilder sb = new StringBuilder();
            BufferedReader reader;

            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String nextLine = "";
                while ((nextLine = reader.readLine()) != null) {
                    sb.append(nextLine);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }
        /*
               * Before the execution of doInBackground, you can do things here.
               * On UI thread
               * @see android.os.AsyncTask#onPreExecute()
               */
        @Override
        protected void onPreExecute() {
            //get customer id
            if  (customerID <= 0){//
                try {
                    customerID = Integer.parseInt(customerIdEditText.getText().toString());
                    } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Must Enter Customer Number", Toast.LENGTH_LONG).show();
                }
            }
        }
         /*
         * You should put those long process code inside this method
         * On Background thread
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected Map<String, String> doInBackground(Void... params) {
            URL url;
            HttpURLConnection urlConnection;
            try {
                url = new URL("http://www.thomas-bayer.com/sqlrest/CUSTOMER/" + customerID );
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                if(urlConnection.getResponseCode() == 404) {
                    notthere = true;
                    return customerInfo;
                }else {
                    customerInfo = parseXML(urlConnection.getInputStream());
                    InputStream in = urlConnection.getInputStream();
                }

            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            return customerInfo;
        }

        //xmlparser
        private Map<String, String> parseXML(InputStream is) {

            try {
               boolean done = false;
                // get a new XmlPullParser object from Factory
                XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
                // set input source
                parser.setInput(is, null);
                // get event type
                int eventType = parser.getEventType();
                // process tag while not reaching the end of document
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    switch (eventType){
                        case XmlPullParser.START_DOCUMENT:
                            break;
                        case XmlPullParser.START_TAG:
                           FindElements(parser);
                           break;
                        case XmlPullParser.END_TAG:
                            break;
                    }//end switch
                    eventType = parser.next();
                }//end while

            } catch (XmlPullParserException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return customerInfo;
        }

        private void GetNoCustomer(){
            Toast.makeText(getApplicationContext(), "No Customer Found", Toast.LENGTH_LONG).show();
        }

        private Map<String, String> FindElements(XmlPullParser parser) {
              String tagName = parser.getName();
            try {
                switch (tagName){
                    case FIRSTNAME:
                        customerInfo.put("FirstName", parser.nextText());
                    break;
                case LASTNAME:
                    customerInfo.put("LastName",parser.nextText());
                    break;
                case STREET:
                    customerInfo.put("Street", parser.nextText());
                    break;
                case CITY:
                    customerInfo.put("City",parser.nextText());
                    break;
                }//end switch

                } catch (XmlPullParserException e) {
                e.printStackTrace();
                } catch (IOException e) {
                e.printStackTrace();
            }
            return customerInfo;
        }

        protected void onPostExecute(Map<String, String> results){
            if (notthere){
                GetNoCustomer();
                customerID = -1;
            }
            if (results != null){
                customerNumTextView.setText(String.valueOf(customerID));
                customerStreetEditText.setText(results.get("Street"));
                customerCityEditText.setText(results.get("City"));
                customerFirstNameEditText.setText(results.get("FirstName"));
                customerLastNameEditText.setText(results.get("LastName"));
            }
            //enable button clear hashmap
            customerInfo.clear(); //clear hashmap
            if(customerID == -1)
                customerNumTextView.setText("");
            customerID = -1;
            pullBtn.setEnabled(true); //enable button
            customerIdEditText.setEnabled(true); //enable text Edit
            customerIdEditText.setText("");
        }
    }//end class LoadWebURL
}
